﻿using ApiTesting.SpecFlow.Helpers;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using Tranquire;

namespace ApiTesting.SpecFlow.Screenplay.Actions
{
    public class SubscribeToAction : ActionUnit<WebsocketService>
    {
        public override string Name => "Subscribe to action";

        public string Symbol { get; private set; }

        public SubscribeToAction(string symbol)
        {
            Symbol = symbol;
        }

        protected override void ExecuteWhen(IActor actor, WebsocketService ability)
        {
            dynamic message = new ExpandoObject();
            message.type = "subscribe";
            message.symbol = Symbol;

            NewtonsoftJsonSerializer serializer = new NewtonsoftJsonSerializer();
            string value = serializer.Serialize(message);

            ability.Send(value);
        }

        public static SubscribeToAction Instance(string symbol) => new SubscribeToAction(symbol);
    }
}
