﻿using ApiTesting.SpecFlow.Screenplay.Actions;
using ApiTesting.SpecFlow.Screenplay.Questions;
using System;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using FluentAssertions;
using Websocket.Client;

namespace ApiTesting.SpecFlow.Steps
{
    [Binding]
    public class WebsocketSteps : StepsBase
    {
        public WebsocketSteps(ScenarioContext context) : base(context)
        {
        }

        [Given(@"I have connected to the websocket")]
        public async Task GivenIHaveConnectedToTheWebsocket()
        {
           await Context.Client().Given(ConnectWebsocketClient.Instance);
        }
        
        [When(@"I subscribe to see (.*) stock values")]
        public void WhenISubscribeToSeeACNStockValues(string symbol)
        {
            Context.Client().When(SubscribeToAction.Instance(symbol));
        }
        
        [Then(@"the result should be returned to the client")]
        public async Task ThenTheResultShouldBeReturnedToTheClient()
        {
            var result = await Context.Client().AsksFor(LatestStockEvent.Instance);
            result.Should().NotBeNullOrEmpty();   
        }
    }
}
