﻿using ApiTesting.SpecFlow.Helpers;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using Tranquire;

namespace ApiTesting.SpecFlow.Screenplay.Actors
{
    public class Client
    {
        public static IActorFacade Instance => new Actor("ThirdSystem")
                                              .CanUse(RestApi.Instance)
                                              .CanUse(WebsocketService.Instance);
    }
}
