﻿using ApiTesting.SpecFlow.Screenplay.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace ApiTesting.SpecFlow.Hooks
{
    [Binding]
    public sealed class CommonHooks : StepsBase
    {
        public CommonHooks(ScenarioContext context) : base(context)
        {
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            Context.Set(Client.Instance);
        }

        [AfterScenario]
        public void AfterScenario()
        {
        }
    }
}
