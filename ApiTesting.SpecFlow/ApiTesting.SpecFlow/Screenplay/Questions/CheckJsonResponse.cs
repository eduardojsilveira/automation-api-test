﻿using ApiTesting.SpecFlow.Helpers;
using Tranquire;
using System;
using System.IO;
using Manatee.Json.Schema;
using Manatee.Json.Serialization;
using Manatee.Json;

namespace ApiTesting.SpecFlow.Screenplay.Questions
{
    public class CheckJsonResponse : Question<bool, RestApi>
    {
        public override string Name => "json returned";

        public JsonSchema Schema { get; private set; }

        public CheckJsonResponse(string schemaFile)
        {
            string path = $"json-schemas\\{schemaFile}.json";
            if (!File.Exists(path)) throw new FileNotFoundException(nameof(path));
            var jsonText = File.ReadAllText(path);
            Schema = new JsonSerializer().Deserialize<JsonSchema>(JsonValue.Parse(jsonText));
        }

        protected override bool Answer(IActor actor, RestApi ability)
        {
            var result = Schema.Validate(JsonValue.Parse(ability.LastResponse.Content));
            return result.IsValid;
        }

        public static CheckJsonResponse Instance (string schema) => new CheckJsonResponse(schema);
    }
}
