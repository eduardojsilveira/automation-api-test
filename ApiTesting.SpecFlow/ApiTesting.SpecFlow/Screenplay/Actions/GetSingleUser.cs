﻿using ApiTesting.SpecFlow.Helpers;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using Tranquire;

namespace ApiTesting.SpecFlow.Screenplay.Actions
{
    public class GetSingleUser : ActionUnit<RestApi>
    {
        public override string Name => "Get Single user";

        public string Id { get; private set; }

        public GetSingleUser(string id)
        {
            Id = id;
        }

        protected override void ExecuteWhen(IActor actor, RestApi ability)
        {
            var segments = new Dictionary<string, string>
            {
                {"id", Id }
            };
            ability.ExecuteRequest("/users/{id}", Method.GET, segments);
        }

        public static GetSingleUser Instance(string id) => new GetSingleUser(id);
    }
}
