﻿using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace ApiTesting.SpecFlow
{
    public abstract class StepsBase
    {
        public ScenarioContext Context { get; }

        public StepsBase(ScenarioContext context)
        {
            Context = context;
        }
    }
}
