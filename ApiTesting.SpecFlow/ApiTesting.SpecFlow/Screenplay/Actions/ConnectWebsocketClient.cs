﻿using ApiTesting.SpecFlow.Helpers;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tranquire;

namespace ApiTesting.SpecFlow.Screenplay.Actions
{
    public class ConnectWebsocketClient : Action<WebsocketService, Task>
    {
        public override string Name => "Connect to websocket service";

        protected override async Task ExecuteWhen(IActor actor, WebsocketService ability)
        {
            await ability.Connect();
        }

        public static ConnectWebsocketClient Instance => new ConnectWebsocketClient();
    }
}
