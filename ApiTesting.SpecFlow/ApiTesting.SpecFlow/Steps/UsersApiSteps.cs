﻿using ApiTesting.SpecFlow.Helpers;
using ApiTesting.SpecFlow.Screenplay.Actions;
using ApiTesting.SpecFlow.Screenplay.Questions;
using TechTalk.SpecFlow;
using FluentAssertions;
using ApiTesting.SpecFlow.Schemas;

namespace ApiTesting.SpecFlow.Steps
{

    [Binding]
    public class UsersApiSteps : StepsBase
    {
        public UsersApiSteps(ScenarioContext context) : base(context)
        {
        }
        
        [When(@"I send a request")]
        public void WhenISendARequest()
        {
            Context.Client().When(GetAllUsers.Instance);
        }
        
        [When(@"I search for user with id (.*)")]
        public void WhenISearchForUserWithId(string id)
        {
            Context.Client().When(GetSingleUser.Instance(id));
        }
                  
        [When(@"I Create an user with attributes name (.*) and job (.*)")]
        public void WhenICreateAnUserWithAttributesNameAndJob(string name, string job)
        {
            Context.Client().When(CreateUser.Endpoint(name, job));
        }

        [Then(@"the server should return the status (.*)")]
        public void ThenTheServerShouldReturnTheStatusCreated(string status)
        {
            Context.Client().AsksFor(CheckStatusCode.Instance(status)).Should().BeTrue();
        }

        [Then(@"the json user response should be returned according the schema name (.*)")]
        public void ThenTheJsonUserResponseShouldBeReturned(string schemaName)
        {
            Context.Client().AsksFor(CheckJsonResponse.Instance(schemaName)).Should().BeTrue();
        }

    }
}
