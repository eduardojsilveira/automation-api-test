﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Reactive.Linq;
using Websocket.Client;
using NUnit.Framework;
using System.Collections.ObjectModel;
using System.Threading;

namespace ApiTesting.SpecFlow.Helpers
{
    public class WebsocketService
    {

        private readonly Uri url;
        private WebsocketClient client;
        private readonly Func<ClientWebSocket> webSocketOptions = new Func<ClientWebSocket>(() =>
        {
            var clientWs = new ClientWebSocket
            {
                Options =
                {
                     KeepAliveInterval = TimeSpan.FromMilliseconds(100)
                }
            };
            return clientWs;
        });

        private IObservable<ResponseMessage> source => client.MessageReceived.Where(x => x.MessageType == WebSocketMessageType.Text);

        private IObservable<ResponseMessage> lastMessage => source.TakeLast(1);

        public ObservableCollection<ResponseMessage> Messages { get; }

        private WebsocketService()
        {
            var config = new ConfigurationBuilder()
            .AddJsonFile("specflow.json")
            .Build();
            url = new Uri(config["websocketEndpoint"].ToString());
            client = new WebsocketClient(url, webSocketOptions);
            Messages = new ObservableCollection<ResponseMessage>();
        }


        private static Lazy<WebsocketService> lazy = new Lazy<WebsocketService>(() => new WebsocketService());

        public static WebsocketService Instance => lazy.Value;


        public async Task Connect()
        {
            await client.StartOrFail();
        }

        public async Task<bool> Disconnect()
        {
            return await client.StopOrFail(WebSocketCloseStatus.NormalClosure, "Closing client");         
        }

        public void Send(string message)
        {
            Messages.Clear();
            client.Send(message);
            //source.Subscribe(msg => {

            //    TestContext.WriteLine("Message Received "  + msg.Text);
            //    Messages.Add(msg);
            //});  
        }
        public async Task<ResponseMessage> GetLastEvent()
        {
            return await source.TakeLast(1);
        }

        public async Task<string> GetLastMessage()
        {
            
            var result = await source.Take(TimeSpan.FromSeconds(2)).ToList();

            
            foreach (var item in result)
            {
                TestContext.WriteLine("Message received : " + item.Text);
            }

            //TestContext.WriteLine("Message Type " + result.MessageType);
            //TestContext.WriteLine("Message Receiveed " + result.Text);

            return result.First().MessageType == WebSocketMessageType.Text ? result.First().Text : string.Empty;
        }
    }
}
