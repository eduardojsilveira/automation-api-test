﻿using ApiTesting.SpecFlow.Screenplay.Actors;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using Tranquire;

namespace ApiTesting.SpecFlow
{
    public static class BaseContext
    {
        public static IActorFacade Client(this ScenarioContext context)
        {
            return context.Get<IActorFacade>();
        }
    }
}
