﻿Feature: Users API
So that I can manage users
As a Administrador App
I should be able to receive and send users data

Scenario: Listing All Users
When I send a request 
Then the json user response should be returned according the schema name users-schema
And the server should return the status OK 

Scenario: Finding User By Id
When I search for user with id 2
Then the json user response should be returned according the schema name single-user-schema
And the server should return the status OK

Scenario: Finding User By Id that couldn't be found
When I search for user with id 9999
Then the server should return the status NotFound

Scenario Outline: Creating User 
When I Create an user with attributes name <name> and job <job>
Then the server should return the status <status>
And  the json user response should be returned according the schema name <schema-name>
Examples:
| name                   | job             | status  | schema-name        |
| Eduardo                | Developer       | Created | create-user-schema |
| Eduardo Júnio Silveira | Project Manager | Created | create-user-schema |