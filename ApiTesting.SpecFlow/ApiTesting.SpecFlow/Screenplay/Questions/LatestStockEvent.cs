﻿using ApiTesting.SpecFlow.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Tranquire;
using System.Threading.Tasks;
using Websocket.Client;

namespace ApiTesting.SpecFlow.Screenplay.Questions
{
    public class LatestStockEvent : Question<Task<string>,WebsocketService>
    {
        public override string Name => "read latest message";

        protected override async Task<string> Answer(IActor actor, WebsocketService ability)
        {
            return await ability.GetLastMessage();
        }

        public static LatestStockEvent Instance => new LatestStockEvent();
    }
}
