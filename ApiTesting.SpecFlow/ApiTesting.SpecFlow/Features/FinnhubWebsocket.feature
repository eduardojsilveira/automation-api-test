﻿Feature: FinnhubWebsocket
	In order to integrate my app with stocks
	As a client 
	I want to notified about stock market

@Websocket
Scenario: Subscribe stock market
	Given I have connected to the websocket
	When I subscribe to see BINANCE:BTCUSDT stock values
	Then the result should be returned to the client
