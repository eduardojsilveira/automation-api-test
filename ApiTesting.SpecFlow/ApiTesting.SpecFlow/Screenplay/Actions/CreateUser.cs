﻿using ApiTesting.SpecFlow.Helpers;
using ApiTesting.SpecFlow.Screenplay.Actors;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using Tranquire;

namespace ApiTesting.SpecFlow.Screenplay.Actions
{
    public class CreateUser : ActionUnit<RestApi>
    {

        public string UserName { get; private set; }
        public string Job { get; private set; }

        public CreateUser(string name, string job)
        {
            UserName = name;
            Job = job;
        }

        public override string Name => "Create a user in rest api";

        protected override void ExecuteWhen(IActor actor, RestApi ability)
        {
            dynamic jsonBody = new ExpandoObject();
            jsonBody.name = UserName;
            jsonBody.job = Job;
            ability.ExecuteRequest("/users", Method.POST,null,null, jsonBody);
        }

        public static CreateUser Endpoint (string name, string job) => new CreateUser(name, job);
    }
}
