﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using RestSharp.Serialization;

namespace ApiTesting.SpecFlow.Helpers
{
    public class NewtonsoftJsonSerializer : IRestSerializer
    {

        private JsonSerializerSettings serializerSettings;


        public string[] SupportedContentTypes => new string []{"application/json", "text/json", "text/x-json", "text/javascript", "*+json"};

        public DataFormat DataFormat => DataFormat.Json;

        public string ContentType { get; set; } = "application/json";


        public NewtonsoftJsonSerializer()
        {
            serializerSettings = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy(),
                }
            };
            
        }

        public T Deserialize<T>(IRestResponse response)
        {
            return JsonConvert.DeserializeObject<T>(response.Content);
        }

        public string Serialize(Parameter parameter)
        {
            return JsonConvert.SerializeObject(parameter.Value, serializerSettings);
        }

        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, serializerSettings);
        }
    }
}
