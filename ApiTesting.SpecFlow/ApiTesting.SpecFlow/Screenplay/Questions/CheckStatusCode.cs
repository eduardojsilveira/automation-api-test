﻿using ApiTesting.SpecFlow.Helpers;
using ApiTesting.SpecFlow.Screenplay.Actors;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Tranquire;

namespace ApiTesting.SpecFlow.Screenplay.Questions
{
    public class CheckStatusCode : Question<bool,RestApi>
    {
        public string Status { get; private set; }
        public override string Name => "Check the last response";

        public CheckStatusCode (string status)
        {
            Status = status ?? throw new ArgumentNullException(nameof(status));
        }

        protected override bool Answer(IActor actor, RestApi ability)
        {
            var statusExpected = Enum.Parse<HttpStatusCode>(Status);
            return ability.LastResponse.StatusCode.Equals(statusExpected);
        }

        public static CheckStatusCode Instance(string status) => new CheckStatusCode(status);
    }
}
