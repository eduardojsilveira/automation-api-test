﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ApiTesting.SpecFlow.Helpers
{
    public class RestApi
    {
        public string Endpoint { get; private set; }

        public IRestResponse LastResponse { get; private set; }

        private static Lazy<RestApi> lazy = new Lazy<RestApi>(() => new RestApi());

        public static RestApi Instance => lazy.Value;

        private RestClient restClient = null;

        private RestApi()
        {
            var config = new ConfigurationBuilder()
            .AddJsonFile("specflow.json")
            .Build();
            Endpoint = config["restEndpoint"].ToString();
            restClient = new RestClient(Endpoint);
        }

        public IRestResponse ExecuteRequest(string action, Method method, Dictionary<string, string> segments = null, Dictionary<string, string> queryParameters = null, object jsonBody = null)
        {
            if (string.IsNullOrEmpty(action)) throw new ArgumentNullException(nameof(action));
            RestRequest request = new RestRequest(action, method);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            if (segments != null)
            {
                foreach (var segment in segments)
                    request.AddUrlSegment(segment.Key, segment.Value);
            }

            if(queryParameters != null)
            {
                foreach (var parameter in queryParameters)
                    request.AddQueryParameter(parameter.Key, parameter.Value);
            }

            if(jsonBody != null)
            {
                request.AddJsonBody(jsonBody);
            }

            LastResponse = restClient.Execute(request);
            return LastResponse;
        }
    }
}
