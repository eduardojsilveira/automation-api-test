﻿using ApiTesting.SpecFlow.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using Tranquire;

namespace ApiTesting.SpecFlow.Screenplay.Actions
{
    public class GetAllUsers : ActionUnit<RestApi>
    {
        public override string Name => "Get All Users";

        protected override void ExecuteWhen(IActor actor, RestApi ability)
        {
            var parameters = new Dictionary<string, string>()
            {
                {"page", "1"}
            };
            ability.ExecuteRequest("/users", RestSharp.Method.GET, null, parameters);
        }

        public static GetAllUsers Instance => new GetAllUsers();
    }
}
